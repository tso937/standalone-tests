import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    mavenCentral()
}

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.6.21"
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

dependencies {
    implementation("org.junit.platform:junit-platform-launcher:1.8.2")
    implementation("org.junit.jupiter:junit-jupiter:5.8.2")
}


tasks {
    withType<Test> {
        useJUnitPlatform()
    }

    withType<Jar> {
        manifest {
            attributes["Main-Class"] = "com.standalone_tests.RunJunitTestsKt"
        }
    }

    withType<ShadowJar> {
        from(sourceSets.main.get().output, sourceSets.test.get().output)
        archiveBaseName.set("standalone-tests")
        archiveClassifier.set("")
        archiveVersion.set("")
    }
}