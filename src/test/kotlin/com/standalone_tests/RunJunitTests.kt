package com.standalone_tests

import org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage
import org.junit.platform.engine.discovery.PackageSelector
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder
import org.junit.platform.launcher.core.LauncherFactory
import org.junit.platform.launcher.listeners.LoggingListener
import org.junit.platform.launcher.listeners.SummaryGeneratingListener
import org.junit.platform.launcher.listeners.TestExecutionSummary
import java.io.PrintWriter
import java.util.logging.Level
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    val loggingListener = LoggingListener.forJavaUtilLogging(Level.INFO)
    val summaryListener = SummaryGeneratingListener()

    val testSuiteSelectors = args.firstOrNull()?.let { TestSuite.valueOf(it).selectors }
        ?: TestSuite.ALL

    println("Running test suites from the following packages:")
    println(testSuiteSelectors.map { it.packageName })
    println()

    val request = LauncherDiscoveryRequestBuilder.request()
        .selectors(testSuiteSelectors)
        .build()

    val launcher = LauncherFactory.create()
    launcher.registerTestExecutionListeners(loggingListener, summaryListener)
    launcher.execute(request)

    summaryListener.summary.printTo(PrintWriter(System.out))

    if (summaryListener.summary.testsFailedCount > 0)
        printFailedTestsAndFail(summaryListener.summary.failures)
}

private fun printFailedTestsAndFail(failures: MutableList<TestExecutionSummary.Failure>) {
    println("Failed tests:")
    failures.forEach {
        println("${it.testIdentifier.displayName}: ${it.exception.message}")
    }

    exitProcess(1)
}

private enum class TestSuite(val selectors: List<PackageSelector>) {
    ACCEPTANCE(listOf(selectPackage("com.standalone_tests.acceptance")));

    companion object {
        val ALL = values().flatMap { it.selectors }
    }
}